package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class Fridge implements IFridge {
	
	int max_size = 20; 
	List<FridgeItem> itemsInFridge;
	
	//constructor 
    public Fridge () {
        max_size = 20;
        itemsInFridge = new ArrayList <FridgeItem>();
    }


	
	public int totalSize() {
		return max_size;
	}

	@Override
	public int nItemsInFridge() {
		return itemsInFridge.size();
	}

	@Override
	public boolean placeIn(FridgeItem item) { 
        if (itemsInFridge.size() < 20) {
        	itemsInFridge.add(item);
            return true;
        }
        else 
            return false;    
	}

	@Override
	public void takeOut(FridgeItem item) {
		if (!(itemsInFridge.contains(item))) {
            throw new NoSuchElementException ();
        }
        else {
        	itemsInFridge.remove(item);
        }
	}

	@Override
	public void emptyFridge() { 
		itemsInFridge.clear();
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		List<FridgeItem> expiredItem = new ArrayList<FridgeItem>();

        for (FridgeItem item : itemsInFridge){

            if (item.hasExpired()){
            	expiredItem.add(item);
            }
        }
        itemsInFridge.removeAll(expiredItem);
        return expiredItem;
    }


	
}
